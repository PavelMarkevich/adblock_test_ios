
import UIKit

final class GradientButton: UIButton {
    private lazy var gradientLayer: CAGradientLayer = {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.bounds
        gradientLayer.colors = [gradientColor.left.cgColor, gradientColor.right.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1, y: 0.5)
        layer.insertSublayer(gradientLayer, at: 0)
        layer.masksToBounds = true
        return gradientLayer
    }()
    
    var gradientColor: GradientColor = Color.clearGradient {
        didSet {
            gradientLayer.colors = [gradientColor.left.cgColor, gradientColor.right.cgColor]
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        gradientLayer.frame = bounds
    }
}
