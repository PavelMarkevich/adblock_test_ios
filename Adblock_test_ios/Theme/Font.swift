
import UIKit

final class Font {
    static let systemLight15 = UIFont.systemFont(ofSize: 15, weight: .light)
    
    static let systemRegular15 = UIFont.systemFont(ofSize: 15, weight: .regular)
    
    static let systemBold20 = UIFont.systemFont(ofSize: 20, weight: .bold)
    
    static let roboboRegular20 = UIFont(name: "Robobo-Regular", size: 20) ?? UIFont.systemFont(ofSize: 20, weight: .regular)
}
