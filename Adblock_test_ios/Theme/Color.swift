
import UIKit

struct GradientColor {
    let left: UIColor
    let right: UIColor
}

final class Color {
    static let clear = UIColor.clear
    static let white = UIColor.white
    static let lightGray = UIColor(red: 0.946, green: 0.946, blue: 0.946, alpha: 1)
    static let dirtyWhite = UIColor(red: 0.965, green: 0.965, blue: 0.965, alpha: 1)
    static let interiorGray = UIColor(red: 0.938, green: 0.938, blue: 0.938, alpha: 1)
    static let gray = UIColor(red: 0.363, green: 0.363, blue: 0.363, alpha: 1)
    static let purple = UIColor(red: 0.752, green: 0.561, blue: 0.996, alpha: 1)
    static let pink = UIColor(red: 0.941, green: 0.498, blue: 1, alpha: 1)
    static let hotPink = UIColor(red: 1, green: 0.333, blue: 0.471, alpha: 1)
    
    static let clearGradient = GradientColor(left: clear, right: clear)
    static let pinkGradient = GradientColor(left: purple, right: pink)
}
