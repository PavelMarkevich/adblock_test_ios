
import SoftUIView

final class NotActivatedView: UIView {
    let menuButton = UIButton()
    let infoButton = UIButton()
    let imageView = UIImageView()
    let segmentedControl = SegmentedControl()
    let activateContainer = SoftUIView()
    let activateButton = GradientButton()
    let dataContainer = SoftUIView()
    let attentionImageView = UIImageView()
    let surfindLabel = UILabel()
    let descriptionLabel = UILabel()
    let checkContainer = SoftUIView()
    let updateLabel = UILabel()
    let updateButton = GradientButton()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
        setupLayout()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
        setupLayout()
    }
}

private extension NotActivatedView {
    func setupView() {
        layer.backgroundColor = Color.dirtyWhite.cgColor
        
        menuButton.setBackgroundImage(#imageLiteral(resourceName: "settings"), for: .normal)
        addSubview(menuButton)

        infoButton.setBackgroundImage(#imageLiteral(resourceName: "info"), for: .normal)
        addSubview(infoButton)
        
        imageView.image = #imageLiteral(resourceName: "shield")
        imageView.contentMode = .scaleAspectFill
        addSubview(imageView)

        segmentedControl.segments = ["Security", "Speed Up", "Manual"]
        addSubview(segmentedControl)
        
        activateContainer.type = .normal
        activateContainer.backgroundColor = Color.dirtyWhite
        activateContainer.cornerRadius = 30
        activateContainer.layer.cornerRadius = 30
        addSubview(activateContainer)
        
        activateButton.gradientColor = Color.pinkGradient
        activateButton.layer.cornerRadius = 24
        activateButton.setImage(#imageLiteral(resourceName: "protected"), for: .normal)
        activateButton.setTitleColor(Color.white, for: .normal)
        activateButton.setTitle("Activate Protection", for: .normal)
        activateButton.bringSubviewToFront(activateButton.imageView!)
        activateButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 20)
        activateButton.titleLabel?.font = Font.roboboRegular20
        activateContainer.addSubview(activateButton)
        
        dataContainer.backgroundColor = Color.lightGray
        dataContainer.type = .normal
        dataContainer.cornerRadius = 30
        addSubview(dataContainer)
        
        attentionImageView.image = #imageLiteral(resourceName: "attention")
        dataContainer.addSubview(attentionImageView)
        
        let boldAttr: [NSAttributedString.Key: Any] = [.font: Font.systemBold20]
        let regularAttr: [NSAttributedString.Key: Any] = [.font: Font.systemRegular15]

        var result = NSMutableAttributedString()
        result = NSMutableAttributedString(string: "Web Surfing is not protected", attributes: boldAttr)
        result.addAttributes(regularAttr, range: result.mutableString.range(of: " is not protected"))
        
        surfindLabel.attributedText = result
        surfindLabel.textColor = Color.hotPink
        surfindLabel.textAlignment = .center
        dataContainer.addSubview(surfindLabel)
        
        descriptionLabel.text = "Some of the websites could contain exploits and viruses and be harmful for your system or your personal data.\nOur database of the harmfull sites getting updates daily."
        descriptionLabel.textColor = Color.gray
        descriptionLabel.font = Font.systemLight15
        descriptionLabel.numberOfLines = 0
        descriptionLabel.lineBreakMode = .byWordWrapping
        descriptionLabel.textAlignment = .center
        dataContainer.addSubview(descriptionLabel)
        
        checkContainer.backgroundColor = Color.interiorGray
        checkContainer.type = .normal
        checkContainer.isSelected = true
        checkContainer.layer.cornerRadius = 30
        checkContainer.layer.masksToBounds = true
        checkContainer.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        checkContainer.layer.borderWidth = 5
        checkContainer.layer.borderColor = Color.lightGray.cgColor
        dataContainer.addSubview(checkContainer)
        
        updateLabel.text = "Chek Database Updates"
        updateLabel.font = Font.systemRegular15
        updateLabel.textColor = Color.gray
        checkContainer.addSubview(updateLabel)
        
        updateButton.gradientColor = Color.pinkGradient
        updateButton.layer.cornerRadius = 16
        updateButton.setImage(#imageLiteral(resourceName: "update"), for: .normal)
        updateButton.setTitleColor(Color.white, for: .normal)
        updateButton.bringSubviewToFront(updateButton.imageView!)
        
        checkContainer.addSubview(updateButton)
    }
    
    func setupLayout() {
        var inset = UIApplication.shared.windows.first?.safeAreaInsets ?? .zero
        if inset == .zero { inset = .init(top: 20, left: 0, bottom: 20, right: 0) }
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.widthAnchor.constraint(equalToConstant: 135).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 135).isActive = true
        imageView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        imageView.topAnchor.constraint(equalTo: menuButton.bottomAnchor, constant: 4).isActive = true
        
        menuButton.translatesAutoresizingMaskIntoConstraints = false
        menuButton.widthAnchor.constraint(equalToConstant: 22).isActive = true
        menuButton.heightAnchor.constraint(equalToConstant: 15).isActive = true
        menuButton.leftAnchor.constraint(equalTo: leftAnchor, constant: 23).isActive = true
        menuButton.topAnchor.constraint(equalTo: topAnchor, constant: inset.top - 1).isActive = true

        infoButton.translatesAutoresizingMaskIntoConstraints = false
        infoButton.widthAnchor.constraint(equalToConstant: 22).isActive = true
        infoButton.heightAnchor.constraint(equalToConstant: 23).isActive = true
        infoButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -23).isActive = true
        infoButton.topAnchor.constraint(equalTo: topAnchor, constant: inset.top - 4).isActive = true
        
        segmentedControl.translatesAutoresizingMaskIntoConstraints = false
        segmentedControl.heightAnchor.constraint(equalToConstant: 61).isActive = true
        segmentedControl.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 28).isActive = true
        segmentedControl.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 24).isActive = true
        segmentedControl.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -23).isActive = true
        
        activateContainer.translatesAutoresizingMaskIntoConstraints = false
        activateContainer.heightAnchor.constraint(equalToConstant: 61).isActive = true
        activateContainer.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -inset.bottom - 10).isActive = true
        activateContainer.rightAnchor.constraint(equalTo: rightAnchor, constant: -30).isActive = true
        activateContainer.leftAnchor.constraint(equalTo: leftAnchor, constant: 30).isActive = true
        
        activateButton.translatesAutoresizingMaskIntoConstraints = false
        activateButton.topAnchor.constraint(equalTo: activateContainer.topAnchor, constant: 6).isActive = true
        activateButton.leadingAnchor.constraint(equalTo: activateContainer.leadingAnchor, constant: 7).isActive = true
        activateButton.bottomAnchor.constraint(equalTo: activateContainer.bottomAnchor, constant: -6).isActive = true
        activateButton.trailingAnchor.constraint(equalTo: activateContainer.trailingAnchor, constant: -7).isActive = true
        
        dataContainer.translatesAutoresizingMaskIntoConstraints = false
        dataContainer.topAnchor.constraint(equalTo: segmentedControl.bottomAnchor, constant: 35).isActive = true
        dataContainer.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 30).isActive = true
        dataContainer.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -30).isActive = true
        dataContainer.bottomAnchor.constraint(equalTo: activateContainer.topAnchor, constant: -16).isActive = true
        
        attentionImageView.translatesAutoresizingMaskIntoConstraints = false
        attentionImageView.topAnchor.constraint(equalTo: dataContainer.topAnchor, constant: 33).isActive = true
        attentionImageView.centerXAnchor.constraint(equalTo: dataContainer.centerXAnchor).isActive = true
        attentionImageView.heightAnchor.constraint(equalToConstant: 78.68).isActive = true
        attentionImageView.widthAnchor.constraint(equalToConstant: 84.26).isActive = true
        
        surfindLabel.translatesAutoresizingMaskIntoConstraints = false
        surfindLabel.topAnchor.constraint(equalTo: attentionImageView.bottomAnchor, constant: 19.32).isActive = true
        surfindLabel.leadingAnchor.constraint(equalTo: dataContainer.leadingAnchor, constant: 29).isActive = true
        surfindLabel.trailingAnchor.constraint(equalTo: dataContainer.trailingAnchor, constant: -26).isActive = true
        surfindLabel.heightAnchor.constraint(equalToConstant: 24).isActive = true
        
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        descriptionLabel.leadingAnchor.constraint(equalTo: dataContainer.leadingAnchor, constant: 15).isActive = true
        descriptionLabel.trailingAnchor.constraint(equalTo: dataContainer.trailingAnchor, constant: -13).isActive = true
        descriptionLabel.bottomAnchor.constraint(equalTo: checkContainer.topAnchor, constant: -24).isActive = true
        descriptionLabel.topAnchor.constraint(equalTo: surfindLabel.bottomAnchor, constant: 27).isActive = true
        
        checkContainer.translatesAutoresizingMaskIntoConstraints = false
        checkContainer.leadingAnchor.constraint(equalTo: dataContainer.leadingAnchor).isActive = true
        checkContainer.trailingAnchor.constraint(equalTo: dataContainer.trailingAnchor).isActive = true
        checkContainer.bottomAnchor.constraint(equalTo: dataContainer.bottomAnchor).isActive = true
        checkContainer.heightAnchor.constraint(equalToConstant: 72).isActive = true
        
        updateLabel.translatesAutoresizingMaskIntoConstraints = false
        updateLabel.leadingAnchor.constraint(equalTo: checkContainer.leadingAnchor, constant: 22).isActive = true
        updateLabel.centerYAnchor.constraint(equalTo: checkContainer.centerYAnchor).isActive = true
        
        updateButton.translatesAutoresizingMaskIntoConstraints = false
        updateButton.widthAnchor.constraint(equalToConstant: 32).isActive = true
        updateButton.heightAnchor.constraint(equalToConstant: 32).isActive = true
        updateButton.rightAnchor.constraint(equalTo: checkContainer.rightAnchor, constant: -18).isActive = true
        updateButton.centerYAnchor.constraint(equalTo: checkContainer.centerYAnchor).isActive = true
    }
}
