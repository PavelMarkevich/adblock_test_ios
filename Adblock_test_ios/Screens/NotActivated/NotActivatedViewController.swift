
import UIKit

class NotActivatedViewController: UIViewController {
    private let contentView = NotActivatedView()
    
    override var prefersStatusBarHidden: Bool {
         return true
    }
    
    override func loadView() {
        super.loadView()
        view = contentView
    }
}
