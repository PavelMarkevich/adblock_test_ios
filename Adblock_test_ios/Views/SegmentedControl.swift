
import SoftUIView

final class SegmentedControl: SoftUIView {
    private var stackView = UIStackView()
    private var segmentViews: [GradientButton] = []
    
    var segments: [String] = [] {
        didSet { updateView() }
    }
        
    override init(frame: CGRect) {
        super.init(frame: frame)
        configurateView()
        updateView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        configurateView()
        updateView()
    }
}

private extension SegmentedControl {
    func configurateView() {
        backgroundColor = Color.lightGray
        type = .normal
        cornerRadius = 30
        layer.cornerRadius = 30
        layer.borderWidth = 5
        layer.borderColor = Color.dirtyWhite.cgColor
        
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.spacing = 10
        addSubview(stackView)
                
        let inset: CGFloat = 10
        stackView.topAnchor.constraint(equalTo: topAnchor, constant: inset).isActive = true
        stackView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -inset).isActive = true
        stackView.leftAnchor.constraint(equalTo: leftAnchor, constant: inset).isActive = true
        stackView.rightAnchor.constraint(equalTo: rightAnchor, constant: -inset).isActive = true
    }
    
    func configurateSegmentView(withTitle title: String) -> GradientButton {
        let segmentView = GradientButton(type: .custom)
        segmentView.layer.cornerRadius = 20
        segmentView.setTitle(title, for: .normal)
        segmentView.addTarget(self, action: #selector(selectSegment(sender:)), for: .touchUpInside)
        return segmentView
    }
    
    func updateView() {
        segmentViews.removeAll()
        segmentViews = segments.map { return configurateSegmentView(withTitle: $0) }
        if let segment = segmentViews.first { selectSegment(sender: segment) }
        
        stackView.arrangedSubviews.forEach { segmentView in
            stackView.removeArrangedSubview(segmentView)
            segmentView.removeFromSuperview()
        }
        segmentViews.forEach { stackView.addArrangedSubview($0) }
    }
    
    @objc func selectSegment(sender: GradientButton) {
        segmentViews.enumerated().forEach { index, button in
            button.gradientColor = button == sender ? Color.pinkGradient : Color.clearGradient
            button.titleLabel?.font = button == sender ? Font.systemRegular15 : Font.systemLight15
            button.setTitleColor(button == sender ? Color.white : Color.gray, for: .normal)
        }
    }
}
